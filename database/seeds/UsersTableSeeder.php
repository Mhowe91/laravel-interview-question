<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeder
	 */
    public function run()
    {
	    DB::table('users')->insert([
	    	'encoded_id' => Uuid::generate(),
		    'name' => 'Nick Petryna',
		    'email' => 'nick@welbi.co',
		    'password' => bcrypt('leafsAreGoingGolfingFML')
	    ]);
    }
}
