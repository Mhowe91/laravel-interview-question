<?php
/**
 * Created by PhpStorm.
 * User: matthowe
 * Date: 2018-04-24
 * Time: 1:17 AM
 */

namespace App;

use Webpatser\Uuid\Uuid;

trait Uuids
{

	/**
	 * Laravel boot function
	 */
	protected static function boot()
	{
		parent::boot();

		static::creating(function ($model) {
			$model->{$model->getKeyName()} = Uuid::generate()->string;
		});
	}
}