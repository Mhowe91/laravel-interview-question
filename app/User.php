<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Uuids, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	/**
	 * Set primary key.
	 *
	 * @var string
	 */
    protected $primaryKey = 'encoded_id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
    protected $casts = [
    	'encoded_id' => 'string',
	    'name' => 'string',
	    'email' => 'string',
	    'password' => 'string',
	    'created_at' => 'timestamp',
	    'updated_at' => 'timestamp',
	    'deleted_at' => 'timestamp'
    ];

}
