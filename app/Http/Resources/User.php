<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

	    $created = Carbon::createFromTimestamp($this->created_at);
	    $updated = Carbon::createFromTimestamp($this->updated_at);

    	return [
    		'encoded_id' => $this->encoded_id,
		    'name' => $this->name,
		    'email' => $this->email,
		    'password' => 'Haha nice try!',
    		'created_at' => $created->diffForHumans(),
    		'updated_at' => $updated->diffForHumans()
	    ];
    }
}
