<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param User $user
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function index(User $user)
    {
    	//Return user list, paginated 5 at a time
	    return response()->json($user->paginate(5), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	//Set validation rules
	    $rules = [
		    'email' => 'required|email|unique:users',
		    'name' => 'required|string|max:255',
		    'password' => 'required|string|min:6'
	    ];

	    //Validate request
	    $validator = Validator::make($request->all(), $rules);
	    if ($validator->fails()){
		    return response()->json($validator->errors(), 400);
	    }

	    //Create new user
	    $user = User::create([
	    	'name' => $request->name,
		    'email' => $request->email,
		    'password' => bcrypt($request->password)
	    ]);

	    return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
	    if (is_null($user)){
		    return response()->json(null, 404);
	    }

	    //Transform response
	    $response = new UserResource($user);

	    return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
	    //Set validation rules
	    $rules = [
		    'email' => 'required|email|unique:users,encoded_id',
		    'name' => 'required|string|max:255',
		    'password' => 'required|string|min:6'
	    ];

	    //Validate request
	    $validator = Validator::make($request->all(), $rules);
	    if ($validator->fails()){
		    return response()->json($validator->errors(), 400);
	    }

	    //Update user
	    $user->update([
		    'name' => $request->name,
		    'email' => $request->email,
		    'password' => bcrypt($request->password)
	    ]);

	    return response()->json($user, 200);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
    public function destroy(User $user)
    {
	    $user->delete();

	    return response()->json(null, 204);
    }
}
