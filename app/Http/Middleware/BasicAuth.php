<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BasicAuth
{
    /**
     * Authenticate request
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	//Implemented basic auth for the sake of time, otherwise, could typically leverage Passport and OAuth2
        return Auth::onceBasic() ?: $next($request);
    }
}
